import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from datetime import datetime as dt

import time

# inspired from https://gitlab.com/JohnPowow/animalwatch/-/blob/main/frontend/gui_tests/runSeleniumTests.py

import sys

# PATH = "./frontend/gui_tests/chromedriver.exe"
URL = "https://www.concertfor.me/"
SEARCHBAR_CSS = "input:nth-child(1)"

CONCERTS_NAME_INDEX = 0
CONCERTS_VENUE_INDEX = 1
CONCERTS_ADDRESS_INDEX = 2
CONCERTS_ATTENDANCE_INDEX = 3
CONCERTS_ARTISTS_INDEX = 4
CONCERTS_DATE_INDEX = 5
CONCERTS_TIME_INDEX = 6
CONCERTS_TIMEZONE_INDEX = 7

ARTISTS_NAME_INDEX = 0
ARTISTS_GENDER_INDEX = 1
ARTISTS_BIRTHDAY_INDEX = 2
ARTISTS_COUNTRY_INDEX = 3
ARTISTS_GENRE_INDEX = 4
ARTISTS_FOLLOWERS_INDEX = 5

SONGS_NAME_INDEX = 0
SONGS_ARTIST_INDEX = 1
SONGS_ALBUM_INDEX = 2
SONGS_GENRE_INDEX = 3
SONGS_RELEASE_DATE_INDEX = 4
SONGS_LENGTH_INDEX = 5
SONGS_PRICE_INDEX = 6

PRIMARY_WAIT_TIME = 2
SECONDARY_WAIT_TIME = 1


class SeleniumTests(unittest.TestCase):
    def setUp(self):
        testoptions = Options()
        testoptions.add_argument("--headless")
        testoptions.add_argument("--no-default-browser-check")
        testoptions.add_argument("--no-sandbox")
        testoptions.add_argument("--disable-gpu")
        testoptions.add_argument("--no-sandbox")
        testoptions.add_argument("--disable-dev-shm-usage")

        self.driver = webdriver.Chrome(options=testoptions)  # service=service
        self.driver.get(URL)
        self.driver.maximize_window()
        self.driver.set_window_size(1920, 1080)

    def tearDown(self):
        self.driver.quit()

    # Test 1: test successful loading of the initial page
    def testTitle(self):
        title = self.driver.title
        self.assertEqual(title, "Home")

    def testConcertNavBar(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Concerts")[0]
        newURL = element.get_attribute("href")

        self.assertEqual(newURL, URL + "concerts")

    def testArtistNavBar(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Artists")[0]
        newURL = element.get_attribute("href")

        self.assertEqual(newURL, URL + "artists")

    def testSongNavBar(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Songs")[0]
        newURL = element.get_attribute("href")

        self.assertEqual(newURL, URL + "songs")

    def testHomeNavigation(self):
        self.driver.find_elements(By.LINK_TEXT, "Home")[0].click()
        time.sleep(PRIMARY_WAIT_TIME)
        self.assertEqual(self.driver.title, "Home")

    def testConcertsNavigation(self):
        self.driver.find_elements(By.LINK_TEXT, "Concerts")[0].click()
        self.waitTilElementClickable(getConcertInstanceOnPage(0))
        self.assertEqual(self.driver.title, "Find Concerts")

    def testArtistsNavigation(self):
        self.driver.find_elements(By.LINK_TEXT, "Artists")[0].click()
        self.waitTilElementClickable(getArtistInstanceOnPage(0))
        self.assertEqual(self.driver.title, "Find Artists")

    def testSongsNavigation(self):
        self.driver.find_elements(By.LINK_TEXT, "Songs")[0].click()
        self.waitTilElementClickable(getSongInstanceOnPage(0))
        self.assertEqual(self.driver.title, "Find Songs")

    def testConcertsInstanceNavigation(self):
        self.driver.find_elements(By.LINK_TEXT, "Concerts")[0].click()
        # click first instance in concerts
        concertsInstance = self.waitTilElementClickable(getConcertInstanceOnPage(0))
        concertsInstance.click()
        time.sleep(PRIMARY_WAIT_TIME)
        self.assertEqual(self.driver.title, "Concert Detail")

    def testSongsInstanceNavigation(self):
        self.driver.find_elements(By.LINK_TEXT, "Songs")[0].click()
        # click first instance in concerts
        concertsInstance = self.waitTilElementClickable(getSongInstanceOnPage(0))
        concertsInstance.click()
        time.sleep(PRIMARY_WAIT_TIME)
        self.assertEqual(self.driver.title, "Song Detail")

    def testArtistsInstanceNavigation(self):
        self.driver.find_elements(By.LINK_TEXT, "Artists")[0].click()
        # click first instance in concerts
        artistsInstance = self.waitTilElementClickable(getArtistInstanceOnPage(0))
        artistsInstance.click()
        time.sleep(PRIMARY_WAIT_TIME)
        self.assertEqual(self.driver.title, "Artist Detail")

    # test if concerts is sorted by date in ascending order by default
    def testConcertsDefaultOrder(self):
        self.driver.find_elements(By.LINK_TEXT, "Concerts")[0].click()
        self.waitTilElementClickable(getConcertInstanceOnPage(0))

        firstCardDate = convertDateStringToDate(
            self.getConcertsAttributeElement(0, CONCERTS_DATE_INDEX).text.strip()
        )
        secondCardDate = convertDateStringToDate(
            self.getConcertsAttributeElement(1, CONCERTS_DATE_INDEX).text.strip()
        )
        self.assertLessEqual(firstCardDate, secondCardDate)

    # test concerts keyword search is working properly
    def testConcertsKeyWordSearch(self):
        self.driver.find_elements(By.LINK_TEXT, "Concerts")[0].click()
        self.waitTilElementClickable(getConcertInstanceOnPage(0))
        SEARCH_QUERY = "Miami Music Week"
        self.clickAndSendKeysToSearch(SEARCH_QUERY)
        time.sleep(PRIMARY_WAIT_TIME)
        nameResult = self.getConcertsAttributeElement(
            0, CONCERTS_NAME_INDEX, True
        ).text.strip()
        self.assertEqual(SEARCH_QUERY, nameResult)

    # test concerts sort by ascending is working
    def testConcertsSortbyAscending(self):
        self.driver.find_elements(By.LINK_TEXT, "Concerts")[0].click()
        self.waitTilElementClickable(getConcertInstanceOnPage(0))
        self.selectSortBy("Attendance")
        self.selectOrderBy("Ascending")
        time.sleep(PRIMARY_WAIT_TIME)
        firstCardAttendance = int(
            self.getConcertsAttributeElement(0, CONCERTS_ATTENDANCE_INDEX).text.strip()
        )
        secondCardAttendance = int(
            self.getConcertsAttributeElement(1, CONCERTS_ATTENDANCE_INDEX).text.strip()
        )
        self.assertLessEqual(firstCardAttendance, secondCardAttendance)

    def testConcertsSortbyDescending(self):
        self.driver.find_elements(By.LINK_TEXT, "Concerts")[0].click()
        self.waitTilElementClickable(getConcertInstanceOnPage(0))
        self.selectSortBy("Attendance")
        self.selectOrderBy("Descending")
        time.sleep(PRIMARY_WAIT_TIME)
        firstCardAttendance = int(
            self.getConcertsAttributeElement(0, CONCERTS_ATTENDANCE_INDEX).text.strip()
        )
        secondCardAttendance = int(
            self.getConcertsAttributeElement(1, CONCERTS_ATTENDANCE_INDEX).text.strip()
        )
        self.assertGreaterEqual(firstCardAttendance, secondCardAttendance)

    def testArtistsKeyWordSearch(self):
        self.driver.find_elements(By.LINK_TEXT, "Artists")[0].click()
        self.waitTilElementClickable(getArtistInstanceOnPage(0))
        SEARCH_QUERY = "Kenny Chesney"
        self.clickAndSendKeysToSearch(SEARCH_QUERY)
        time.sleep(PRIMARY_WAIT_TIME)
        nameResult = self.getArtistsAttributeElement(
            0, ARTISTS_NAME_INDEX, True
        ).text.strip()
        self.assertEqual(SEARCH_QUERY, nameResult)

    def testArtistsSortByAscending(self):
        self.driver.find_elements(By.LINK_TEXT, "Artists")[0].click()
        self.waitTilElementClickable(getArtistInstanceOnPage(0))
        self.selectSortBy("Name")
        self.selectOrderBy("Ascending")
        time.sleep(2)
        firstCardName = self.getArtistsAttributeElement(
            0, ARTISTS_NAME_INDEX
        ).text.strip()

        secondCardName = self.getArtistsAttributeElement(
            1, ARTISTS_NAME_INDEX
        ).text.strip()
        self.assertLessEqual(firstCardName, secondCardName)

    def testArtistsSortByDescending(self):
        self.driver.find_elements(By.LINK_TEXT, "Artists")[0].click()
        self.waitTilElementClickable(getArtistInstanceOnPage(0))
        self.selectSortBy("Name")
        self.selectOrderBy("Descending")
        time.sleep(2)
        firstCardName = self.getArtistsAttributeElement(
            0, ARTISTS_NAME_INDEX
        ).text.strip()

        secondCardName = self.getArtistsAttributeElement(
            1, ARTISTS_NAME_INDEX
        ).text.strip()

        self.assertGreaterEqual(firstCardName, secondCardName)

    def testSongsDefaultOrder(self):
        self.driver.find_elements(By.LINK_TEXT, "Songs")[0].click()
        self.waitTilElementClickable(getSongInstanceOnPage(0))
        SEARCH_QUERY = "AE"
        self.clickAndSendKeysToSearch(SEARCH_QUERY)
        time.sleep(PRIMARY_WAIT_TIME)
        firstCardName = self.getSongsAttributeElement(0, SONGS_NAME_INDEX).text.strip()
        secondCardName = self.getSongsAttributeElement(1, SONGS_NAME_INDEX).text.strip()

        self.assertLessEqual(firstCardName, secondCardName)

    def testSongsKeyWordSearch(self):
        self.driver.find_elements(By.LINK_TEXT, "Songs")[0].click()
        self.waitTilElementClickable(getSongInstanceOnPage(0))
        SEARCH_QUERY = "All About That Bass"
        self.clickAndSendKeysToSearch(SEARCH_QUERY)
        time.sleep(PRIMARY_WAIT_TIME)
        nameResult = self.getSongsAttributeElement(
            0, SONGS_NAME_INDEX, True
        ).text.strip()
        self.assertEqual(SEARCH_QUERY, nameResult)

    def testSongsSortByAscending(self):
        self.driver.find_elements(By.LINK_TEXT, "Songs")[0].click()
        self.waitTilElementClickable(getSongInstanceOnPage(0))
        self.selectSortBy("Release Date")
        self.selectOrderBy("Ascending")
        time.sleep(PRIMARY_WAIT_TIME)
        firstCardReleaseDate = convertDateStringToDate(
            self.getSongsAttributeElement(0, SONGS_RELEASE_DATE_INDEX).text.strip()
        )

        secondCardReleaseDate = convertDateStringToDate(
            self.getSongsAttributeElement(1, SONGS_RELEASE_DATE_INDEX).text.strip()
        )
        self.assertLessEqual(firstCardReleaseDate, secondCardReleaseDate)

    def testSongsSortByDescending(self):
        self.driver.find_elements(By.LINK_TEXT, "Songs")[0].click()
        self.waitTilElementClickable(getSongInstanceOnPage(0))
        self.selectSortBy("Release Date")
        self.selectOrderBy("Descending")
        time.sleep(PRIMARY_WAIT_TIME)
        firstCardReleaseDate = convertDateStringToDate(
            self.getSongsAttributeElement(0, SONGS_RELEASE_DATE_INDEX).text.strip()
        )

        secondCardReleaseDate = convertDateStringToDate(
            self.getSongsAttributeElement(1, SONGS_RELEASE_DATE_INDEX).text.strip()
        )

        self.assertGreaterEqual(firstCardReleaseDate, secondCardReleaseDate)

    def testArtistsFilter(self):
        self.driver.find_elements(By.LINK_TEXT, "Artists")[0].click()
        self.waitTilElementClickable(getArtistInstanceOnPage(0))
        self.selectArtistsGender("Male")
        time.sleep(PRIMARY_WAIT_TIME)
        firstCardGender = self.getArtistsAttributeElement(
            0, ARTISTS_GENDER_INDEX
        ).text.strip()

        self.assertEqual(firstCardGender, "Male")

    def testConcertsFilter(self):
        self.driver.find_elements(By.LINK_TEXT, "Concerts")[0].click()
        self.waitTilElementClickable(getConcertInstanceOnPage(0))
        self.selectConcertsTimeZone("America/Chicago")
        time.sleep(PRIMARY_WAIT_TIME)
        firstCardTimeZone = self.getConcertsAttributeElement(
            0, CONCERTS_TIMEZONE_INDEX
        ).text.strip()
        self.assertEqual(firstCardTimeZone, "America/Chicago")

    def selectSortBy(self, sortBy):
        # click sortBy
        self.driver.find_element(
            By.XPATH, "//div[@id='root']/div/div[2]/div/div[2]/div/div"
        ).click()
        # select option
        self.driver.find_element(
            By.XPATH, "//li[contains(.,'{}')]".format(sortBy)
        ).click()

    def selectOrderBy(self, orderBy):
        # click orderBy
        self.driver.find_element(
            By.XPATH, "//div[@id='root']/div/div[2]/div/div[3]/div/div"
        ).click()
        # select option
        self.driver.find_element(
            By.XPATH, "//li[contains(.,'{}')]".format(orderBy)
        ).click()

    def selectConcertsTimeZone(self, timeZone):
        self.driver.find_element(
            By.XPATH, "//div[@id='root']/div/div[2]/div/div[4]/div/div"
        ).click()
        # select option
        self.driver.find_element(
            By.XPATH, "//li[contains(.,'{}')]".format(timeZone)
        ).click()

    def selectArtistsGender(self, gender):
        self.driver.find_element(
            By.XPATH, "//div[@id='root']/div/div[2]/div[2]/div/div/div"
        ).click()
        # select option
        self.driver.find_element(
            By.XPATH, "//li[contains(.,'{}')]".format(gender)
        ).click()

    def selectSongsGenre(self, genre):

        self.driver.find_element(
            By.XPATH, "//div[@id='root']/div/div[2]/div[2]/div[3]/div/div"
        ).click()
        # select option
        self.driver.find_element(
            By.XPATH, "//li[contains(.,'{}')]".format(genre)
        ).click()

    def clickAndSendKeysToSearch(self, searchQuery):
        searchBar = self.driver.find_elements(By.CSS_SELECTOR, SEARCHBAR_CSS)[0]
        searchBar.send_keys(searchQuery)

    def getConcertsAttributeElement(self, cardIndex, attributeIndex, search=False):
        return self.driver.find_elements(
            By.CSS_SELECTOR, getConcertAttributeCSS(cardIndex, attributeIndex, search)
        )[0]

    def getArtistsAttributeElement(self, cardIndex, attributeIndex, search=False):
        return self.driver.find_elements(
            By.CSS_SELECTOR, getArtistAttributeCSS(cardIndex, attributeIndex, search)
        )[0]

    def getSongsAttributeElement(self, cardIndex, attributeIndex, search=False):
        return self.driver.find_elements(
            By.CSS_SELECTOR, getSongAttributeCSS(cardIndex, attributeIndex, search)
        )[0]

    def waitTilElementClickable(self, elementCSS):
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.CSS_SELECTOR, elementCSS))
        )
        return WebDriverWait(self.driver, 5).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, elementCSS))
        )


DATE_MASK = "%Y-%m-%d"
DATE_MASK_2 = "%m/%d/%Y"


def convertDateStringToDate(dateString):
    try:
        return dt.strptime(dateString, DATE_MASK)
    except:
        return dt.strptime(dateString, DATE_MASK_2)


def getConcertInstanceOnPage(index):
    result = ".MuiGrid-root:nth-child({}) > .MuiPaper-root a span > span".format(
        index + 1
    )
    return result


def getSongInstanceOnPage(index):
    result = ".MuiGrid-root:nth-child({}) .card-title span > span".format(index + 1)
    return result


def getArtistInstanceOnPage(index):
    result = ".MuiGrid-root:nth-child({}) .card-title span > span".format(index + 1)
    return result


def getArtistAttributeCSS(cardIndex, attributeIndex, search=False):
    if attributeIndex == 0:
        result = "mark" if search else getArtistInstanceOnPage(cardIndex)
    else:
        indexInCard = 2 + (attributeIndex - 1) * 3
        result = ".MuiGrid-root:nth-child({}) span:nth-child({}) > span".format(
            cardIndex + 1, indexInCard
        )

    return result


def getSongAttributeCSS(cardIndex, attributeIndex, search=False):
    if attributeIndex == 0:
        result = "mark" if search else getSongInstanceOnPage(cardIndex)
    else:
        indexInCard = 2 + (attributeIndex - 1) * 3
        result = ".MuiGrid-root:nth-child({}) span:nth-child({}) > span".format(
            cardIndex + 1, indexInCard
        )

    return result


def getConcertAttributeCSS(cardIndex, attributeIndex, search=False):
    if attributeIndex == 0:
        result = "mark" if search else getConcertInstanceOnPage(cardIndex)
    else:
        indexInCard = 2 + (attributeIndex - 1) * 3
        result = ".MuiGrid-root:nth-child({}) > .MuiPaper-root span:nth-child({}) > span".format(
            cardIndex + 1, indexInCard
        )

    return result


if __name__ == "__main__":
    unittest.main(argv=["first-arg-is-ignored"])
