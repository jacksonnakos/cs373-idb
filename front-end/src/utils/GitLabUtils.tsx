
export interface Counts {
    all: number;
    closed: number;
    opened: number;
}

export interface Statistics {
    counts: Counts;
}

export interface StatisticsCall {
    statistics: Statistics;
}
export interface ContributorInfo {
    name: string;
    email: string;
    commits: number;
    additions: number;
    deletions: number;
}

export function findContributorInfoWithName(contributorInfoList: ContributorInfo[], gitLabName: String) {
    if (contributorInfoList == undefined || contributorInfoList.length == 0) {
        return null
    }
    for (var i = 0; i < contributorInfoList.length; i++) {
        let contributorInfo: ContributorInfo = contributorInfoList[i]
        if (contributorInfo.name === gitLabName) {
            return contributorInfo
        }
    }
    console.log(contributorInfoList)
    throw new Error("Name " + gitLabName + " not found in contributorInfoList!!!");
}

export function getTotalCommits(contributorInfoList: ContributorInfo[]) {
    if (contributorInfoList == undefined || contributorInfoList.length == 0) {
        return 0
    }
    let result = 0
    for (var i = 0; i < contributorInfoList.length; i++) {
        let contributorInfo: ContributorInfo = contributorInfoList[i]
        result += contributorInfo.commits
    }
    return result
}

export function responseToContributorArray(response: string) {
    let result: ContributorInfo[] = JSON.parse(response);
    return result;
}

export function statsCallResponseToTotalNumIssues(myJson: string): number {
    let stats: StatisticsCall = JSON.parse(myJson)
    let result = stats.statistics.counts.all
    return result
}
