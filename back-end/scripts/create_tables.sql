CREATE TABLE songs(
  song_id int PRIMARY KEY,
	artists varchar(50),
	album varchar(30),
  song_name varchar(30),
	artist_view_url text,
	track_view_url text,
	preview_url text,
	artwork_url text,
	price money,
	release_date varchar(30),
  explicit varchar(30),
	track_count int,
	track_number int,
	track_length int,
	country varchar(30),
	currency varchar(30),
	genre varchar(30)
);
SELECT * FROM songs;

CREATE TABLE concerts(
    concert_id int PRIMARY KEY,
	event_name varchar(50),
	event_description text,
	venue_name varchar(50),
	venie_address varchar(50),
	predicted_attendance int,
	start_date varchar(30),
	start_time varchar(30),
	time_zone varchar(30),
	artist_name varchar(30),
	image_url text
);
SELECT * FROM concerts;

CREATE TABLE artists(
    artist_id int PRIMARY KEY,
	artist_name varchar(30),
	spotify_url text,
	followers int,
	popularity int,
	genre varchar(30),
	spotify_uri text,
	bio text,
	birthday varchar(30),
	real_name varchar(30)
);
SELECT * FROM artists;

CREATE TABLE concert_song_junction(
    csj_id int PRIMARY KEY,
    concert_id int,
    song_id int,
	concert_name varchar(50),
	song_name varchar(30),
    FOREIGN KEY (concert_id) REFERENCES concerts(concert_id) ON DELETE CASCADE,
    FOREIGN KEY (song_id) REFERENCES songs(song_id) ON DELETE CASCADE
);
SELECT * FROM concert_song_junction;

CREATE TABLE artist_song_junction(
    asj_id int PRIMARY KEY,
    artist_id int,
    song_id int,
	artist_name varchar(30),
	song_name varchar(30),
    FOREIGN KEY (artist_id) REFERENCES artists(artist_id) ON DELETE CASCADE,
    FOREIGN KEY (song_id) REFERENCES songs(song_id) ON DELETE CASCADE
);
SELECT * FROM artist_song_junction;

CREATE TABLE artist_concert_junction(
    acj_id int PRIMARY KEY,
    artist_id int,
    concert_id int,
	artist_name varchar(30),
	concert_name varchar(50),
    FOREIGN KEY (artist_id) REFERENCES artists(artist_id) ON DELETE CASCADE,
    FOREIGN KEY (concert_id) REFERENCES concerts(concert_id) ON DELETE CASCADE
);
SELECT * FROM artist_concert_junction; 

