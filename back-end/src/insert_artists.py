import psycopg2
import artists_api

# https://pynative.com/python-postgresql-tutorial/#h-python-postgresql-database-connection
# https://www.tutorialspoint.com/python_data_access/python_postgresql_database_connection.htm
conn = psycopg2.connect(
    database="postgres",
    user="Structure3081",
    password="8oCKgqY7Ns6phDZBatQR",
    host="concertformedb.cfvcdcztpu5h.us-east-2.rds.amazonaws.com",
    port="5432",
)

cursor = conn.cursor()


def insert_artists():
    with open("stored_artists.txt", "w") as f:

        # Delete everything from the table
        s = "DELETE FROM artists"
        cursor.execute(s)
        conn.commit()

        artist_names = []

        q = "SELECT concert_id, artist_name, event_name FROM concerts"
        cursor.execute(q)
        data = cursor.fetchall()
        # print("DATA: ", data)
        jid = 1
        id = 1
        for row in data:
            concert_id, artist_name, concert_name = row[0], row[1], row[2]

            q = f"SELECT 1 AS r FROM artists WHERE artist_name = '{artist_name}' LIMIT(1);"
            cursor.execute(q)
            st = cursor.fetchall()
            ins_id = id
            if st == []:
                c = artists_api.get_artists_by_name([artist_name])["artists"][0]
                s = f"INSERT INTO artists VALUES \
                    ({id}, '{c['artist_name']}', '{c['spotify_page_url']}', \
                    {c['followers']}, {c['popularity']}, '{c['genre']}', \
                    '{c['spotify_id']}', '{c['birthday']}', '{c['aliases']}', \
                    '{c['image_url']}', '{c['gender']}', '{c['home_country']}')"
                # print(f'\n\n{s}\n\n')
                if id % 50 == 0 or True:
                    conn.commit()
                    print(f"committed id {id}")
                id += 1
                cursor.execute(s)

    conn.commit()


# Closing the connection
cursor.close()
conn.close()
