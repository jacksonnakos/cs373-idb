"""
This provides an API to provide Song information.
Internally, it relies on the Apple Music API.
https://developer.apple.com/library/archive/documentation/AudioVideo/Conceptual/iTuneSearchAPI/index.html#//apple_ref/doc/uid/TP40017632-CH3-SW1

Rreferences:
"""
import requests


def get_songs(s, n=10):
    url = f"https://itunes.apple.com/search?term={s}&media=music&limit={n}&entity=song"
    response = requests.get(url)
    return response.json()


def test():
    print(get_songs(s="Sicko Mode", n=1))


if __name__ == "__main__":
    test()
