import psycopg2
import concerts_api

# https://pynative.com/python-postgresql-tutorial/#h-python-postgresql-database-connection
# https://www.tutorialspoint.com/python_data_access/python_postgresql_database_connection.htm
conn = psycopg2.connect(
    database="postgres",
    user="Structure3081",
    password="removed",
    host="concertformedb.cfvcdcztpu5h.us-east-2.rds.amazonaws.com",
    port="5432",
)

cursor = conn.cursor()

with open("stored.txt", "w") as f:

    # Delete everything from the table
    # s = "DELETE FROM concerts"
    # cursor.execute(s)
    # conn.commit()

    N = 1000
    id = 1
    offset = 0
    while N > 0:
        num = min(49, N - offset)
        concerts = concerts_api.get_concerts(num, offset)["concerts"]
        N -= num
        offset += num

        for c in concerts:
            try:
                s = f"INSERT INTO concerts VALUES \
                    ({id}, '{c['event_name']}', '{c['event_description']}', \
                    '{c['venue_name']}', '{c['venue_address']}', {c['predicted_attendance']}, \
                    '{c['start_date']}', '{c['start_time']}', '{c['time_zone']}', \
                    '{c['artist_name']}', '{c['image_url']}')"
                f.write(s + "\n")
                cursor.execute(s)
                print(f"id {id} inserted\n")
                id += 1
            except:
                pass
        conn.commit()

# Closing the connection
cursor.close()
conn.close()
