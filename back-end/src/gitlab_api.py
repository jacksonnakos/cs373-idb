"""
This provides an API to provide Gitlab information.
Internally, it relies on the Gitlab API.
https://docs.gitlab.com/ee/

Rreferences:
"""
import requests


def get_commits():
    # Call to retrieve commits
    url = "https://gitlab.com/api/v4/projects/39617716/repository/commits"
    response = requests.get(url)
    return response.json()


def get_issues():
    # Call to retrieve commits
    url = "https://gitlab.com/api/v4/projects/39617716/issues"
    response = requests.get(url)
    return response.json()


def test():
    print(get_commits())
    print(get_issues())


if __name__ == "__main__":
    test()
