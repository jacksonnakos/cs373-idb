from flask import Flask, Response, request

# from logging.config import dictConfig
from flask_cors import CORS
import psycopg2
import collections


############################################
# App and REST API Functions
############################################

NUMERIC_FIELDS = [
    # artists
    "followers",
    "popularity",
    "artist_id",
    # concerts
    "concert_id",
    "predicted_attendance",
    # songs
    "song_id",
    "track_count",
    "track_length",
    "track_number",
]

STRICT_MATCH_FIELDS = [
    # song fields
    "explicit",
    "country",
    "currency",
    "price",
    "release_date",
    "track_number",
    # artist fields
    "gender",
    "birthday",
    "home_country",
    # concerts
]

pgdbpas = "8oCKgqY7Ns6phDZBatQR"
app = Flask(__name__)
CORS(app)
return_header = {"Content-Type": "application/json"}


def get_cursor():
    conn = psycopg2.connect(
        database="postgres",
        user="Structure3081",
        password=pgdbpas,
        host="concertformedb.cfvcdcztpu5h.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    return conn.cursor()


concert_row_names = [
    "concert_id",
    "event_name",
    "event_description",
    "venue_name",
    "venue_address",
    "predicted_attendance",
    "start_date",
    "start_time",
    "time_zone",
    "artist_name",
    "image_url",
]


def make_concert_dict(db_row):
    d = {concert_row_names[i]: db_row[i] for i in range(len(db_row))}
    return d


def get_sort_field_and_order(sortby):
    order = "DESC" if sortby[0] == "-" else "ASC"
    field = sortby
    if field[0] in "-+":
        field = field[1:]
    return field, order


def get_filter_clause(args):
    if "filterby" not in args:
        return ""

    def get_condition(field, query):
        if field in NUMERIC_FIELDS:
            comp = query[0]  # '>' or '<'
            pattern = query[1:]
            return f" {field} {comp} {pattern} "
        elif field in STRICT_MATCH_FIELDS:
            comp = "="
            pattern = query
            return f" {field} {comp} '{pattern}' "
        else:
            comp = "ILIKE"
            pattern = f"%{query}%"
            return f" {field} {comp} '{pattern}' "

    filter_fields = args["filterby"].split(" ")
    filter_qs = args["fquery"].split(" ")

    fdict = collections.defaultdict(list)
    for i in range(len(filter_fields)):
        fdict[filter_fields[i]].append(filter_qs[i])

    filterquery = ""
    for field in fdict:
        join_op = "AND" if field in NUMERIC_FIELDS else "OR"
        i = iter(fdict[field])
        query = next(i)
        filterquery += f" AND ( {get_condition(field, query)} "
        for query in i:
            filterquery += f" {join_op} {get_condition(field, query)} "
        filterquery += ") "
    return filterquery


# /api/concerts?search=a+b+c+d
@app.route("/api/concerts")
def get_concerts(n=100000, offset=0):
    tablename = "concerts"
    args = request.args
    model_name = tablename[:-1]
    get_instance_by_id = eval(f"get_{model_name}_by_id")
    if "id" in args:
        return get_instance_by_id(args["id"])
    if "n" in args:
        n = args["n"]
    if "offset" in args:
        offset = args["offset"]

    pattern = "'%'"
    if "search" in args:
        query = args["search"]
        tokens = query.split(" ")
        pattern = f"'%{'%'.join(tokens)}%'"

    sort_field = f"{model_name}_id"
    order = "ASC"
    if "sortby" in args:
        sort_field, order = get_sort_field_and_order(args["sortby"])

    filterquery = get_filter_clause(args)

    body = f"( SELECT * FROM {tablename} \
            WHERE ( event_name ILIKE {pattern} \
            OR venue_name ILIKE {pattern} \
            OR artist_name ILIKE {pattern} ) \
            {filterquery} )"

    s = f"{body} ORDER BY {sort_field} {order} OFFSET {offset} LIMIT {n}"

    cursor = get_cursor()

    cursor.execute("( SELECT COUNT(1) " + body[10:])
    res = cursor.fetchone()
    if res is None:
        total_rows = 0
    else:
        total_rows = res[0]
    data = {"total_rows": total_rows}

    cursor.execute(s)
    db_rows = cursor.fetchall()
    res_dicts = [eval(f"make_{model_name}_dict")(db_row) for db_row in db_rows]
    data.update({"length": len(res_dicts), tablename: res_dicts})

    return data


@app.route("/api/concerts/<id>")
def get_concert_by_id(id):
    cursor = get_cursor()
    cursor.execute(f"SELECT * FROM concerts WHERE concert_id = {id}")
    data = cursor.fetchall()
    concert_dict = make_concert_dict(data[0])

    q = f"SELECT acj.artist_id, acj.artist_name FROM artist_concert_junction acj\
            WHERE acj.concert_id = {id};"
    cursor.execute(q)
    artists = cursor.fetchall()

    q = f"SELECT csj.song_id, csj.song_name FROM concert_song_junction csj\
            WHERE csj.concert_id = {id};"
    cursor.execute(q)
    songs = cursor.fetchall()

    data = {"concerts": [concert_dict], "songs": songs, "artists": artists}
    cursor.close()
    return data


song_row_names = [
    "song_id",
    "artists",
    "album",
    "song_name",
    "artist_view_url",
    "track_view_url",
    "preview_url",
    "artwork_url",
    "price",
    "release_date",
    "explicit",
    "track_count",
    "track_number",
    "track_length",
    "country",
    "currency",
    "genre",
]


def make_song_dict(db_row):
    d = {song_row_names[i]: db_row[i] for i in range(len(db_row))}
    d["album"] = d["album"].replace('"', "'")
    d["artists"] = d["artists"].replace('"', "'")
    d["song_name"] = d["song_name"].replace('"', "'")
    return d


@app.route("/api/songs")
def get_songs(n=100000, offset=0):
    tablename = "songs"
    args = request.args
    model_name = tablename[:-1]
    get_instance_by_id = eval(f"get_{model_name}_by_id")
    if "id" in args:
        return get_instance_by_id(args["id"])
    if "n" in args:
        n = args["n"]
    if "offset" in args:
        offset = args["offset"]

    pattern = "'%'"
    if "search" in args:
        query = args["search"]
        tokens = query.split(" ")
        pattern = f"'%{'%'.join(tokens)}%'"

    sort_field = f"{model_name}_id"
    order = "ASC"
    if "sortby" in args:
        sort_field, order = get_sort_field_and_order(args["sortby"])

    filterquery = get_filter_clause(args)

    body = f"( SELECT * FROM {tablename} \
            WHERE ( artists ILIKE {pattern} \
            OR album ILIKE {pattern} \
            OR song_name ILIKE {pattern} ) \
            {filterquery} )"

    s = f"{body} ORDER BY {sort_field} {order} OFFSET {offset} LIMIT {n}"

    cursor = get_cursor()

    cursor.execute("( SELECT COUNT(1) " + body[10:])
    res = cursor.fetchone()
    if res is None:
        total_rows = 0
    else:
        total_rows = res[0]
    data = {"total_rows": total_rows}

    cursor.execute(s)
    db_rows = cursor.fetchall()
    res_dicts = [eval(f"make_{model_name}_dict")(db_row) for db_row in db_rows]
    data.update({"length": len(res_dicts), tablename: res_dicts})

    return data


@app.route("/api/songs/<id>")
def get_song_by_id(id):
    cursor = get_cursor()
    cursor.execute(f"SELECT * FROM songs WHERE song_id = {id}")
    data = cursor.fetchall()
    song_dict = make_song_dict(data[0])

    q = f"SELECT asj.artist_id, asj.artist_name FROM artist_song_junction asj\
            WHERE asj.song_id = {id};"
    cursor.execute(q)
    artists = cursor.fetchall()

    q = f"SELECT csj.concert_id, csj.concert_name FROM concert_song_junction csj\
            WHERE csj.song_id = {id};"
    cursor.execute(q)
    concerts = cursor.fetchall()

    data = {"songs": [song_dict], "concerts": concerts, "artists": artists}
    cursor.close()
    return data


artist_row_names = [
    "artist_id",
    "artist_name",
    "spotify_page_url",
    "followers",
    "popularity",
    "genre",
    "spotify_id",
    "birthday",
    "aliases",
    "image_url",
    "gender",
    "home_country",
]


def make_artist_dict(db_row):
    d = {artist_row_names[i]: db_row[i] for i in range(len(db_row))}
    return d


@app.route("/api/artists")
def get_artists(n=100000, offset=0):
    tablename = "artists"
    args = request.args
    model_name = tablename[:-1]
    get_instance_by_id = eval(f"get_{model_name}_by_id")
    if "id" in args:
        return get_instance_by_id(args["id"])
    if "n" in args:
        n = args["n"]
    if "offset" in args:
        offset = args["offset"]

    pattern = "'%'"
    if "search" in args:
        query = args["search"]
        tokens = query.split(" ")
        pattern = f"'%{'%'.join(tokens)}%'"

    sort_field = f"{model_name}_id"
    order = "ASC"
    if "sortby" in args:
        sort_field, order = get_sort_field_and_order(args["sortby"])

    filterquery = get_filter_clause(args)

    body = f"( SELECT * FROM {tablename} \
            WHERE ( artist_name ILIKE {pattern} \
            OR aliases ILIKE {pattern} ) \
            {filterquery} )"

    s = f"{body} ORDER BY {sort_field} {order} OFFSET {offset} LIMIT {n}"
    print(s)
    cursor = get_cursor()

    cursor.execute("( SELECT COUNT(1) " + body[10:])
    res = cursor.fetchone()
    if res is None:
        total_rows = 0
    else:
        total_rows = res[0]
    data = {"total_rows": total_rows}

    cursor.execute(s)
    db_rows = cursor.fetchall()
    res_dicts = [eval(f"make_{model_name}_dict")(db_row) for db_row in db_rows]
    data.update({"length": len(res_dicts), tablename: res_dicts})

    return data


@app.route("/api/artists/<id>")
def get_artist_by_id(id):
    cursor = get_cursor()
    cursor.execute(f"SELECT * FROM artists WHERE artist_id = {id}")
    data = cursor.fetchall()
    artist_dict = make_artist_dict(data[0])

    q = f"SELECT asj.song_id, asj.song_name FROM artist_song_junction asj \
            WHERE asj.artist_id = {id};"
    cursor.execute(q)
    songs = cursor.fetchall()

    q = f"SELECT acj.concert_id, acj.concert_name FROM artist_concert_junction acj \
            WHERE acj.artist_id = {id};"
    cursor.execute(q)
    concerts = cursor.fetchall()

    data = {"artists": [artist_dict], "songs": songs, "concerts": concerts}
    cursor.close()
    return data


############################################
# Run the app
############################################


def main():
    app.debug = True
    # app.logger.info('App started...')
    app.run(host="0.0.0.0", port=5003)


if __name__ == "__main__":
    main()
