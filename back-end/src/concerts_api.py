"""
This provides an API to provide Concert information.
Internally, it relies on the PredictHQ API.
https://control.predicthq.com/
"""
import requests
import json
import time

pmq_tkn = "removed"
bing_token = "removed"


def __get_raw_concerts(n, offset):
    raw_concerts = []
    response = requests.get(
        url="https://api.predicthq.com/v1/events/",
        headers={"Authorization": f"Bearer {pmq_tkn}", "Accept": "application/json"},
        params={
            "sort": "rank",
            "country": "US,CA,GB",
            "limit": n,
            "offset": offset,
            "category": "concerts",
        },
    )
    raw_concerts.extend(response.json()["results"])
    return raw_concerts


"""
CONCERT TABLE:
- id : int
- event_name : char[50]
- description : text
- venue_name : str
- venue_address : str
- predicted_attendance : int
- start_date : str
- start_time : str
- timezone : str
- artist_name : str
- image_url : str 
"""
import artists_api


def get_thumbnail(q):
    url = "https://api.bing.microsoft.com/v7.0/images/search"
    headers = {"Ocp-Apim-Subscription-Key": bing_token}
    params = {"q": q, "count": 1, "mkt": "en-US"}
    response = requests.get(url, params=params, headers=headers)
    data = response.json()
    thumbnail = data["value"][0]["thumbnailUrl"].strip()
    return thumbnail


def __extract_fields(raw_concert):
    rc = raw_concert

    d = {}
    d["event_name"] = rc["title"].replace("'", "").replace('"', "")
    d["event_description"] = (
        rc["description"].replace("'", "").replace('"', "")
        if len(rc["description"].strip()) > 2
        else "Not Provided"
    )
    d["predicted_attendance"] = rc["phq_attendance"]
    ents = rc["entities"]
    venue_name = "Not Provided" if len(ents) == 0 else ents[0]["name"]
    venue_address = "Not Provided" if len(ents) == 0 else ents[0]["formatted_address"]
    d["venue_name"] = venue_name.replace("'", "").replace('"', "")
    d["venue_address"] = venue_address.replace("'", "").replace('"', "")
    d["start_date"] = rc["start"].split("T")[0]
    d["start_time"] = rc["start"].split("T")[1][:-1:]
    # d['end_date'] = rc['end'].split('T')[0]
    # d['end_time'] = rc['end'].split('T')[1][:-1:]
    d["time_zone"] = rc["timezone"]
    name = d["event_name"].replace("'", "").replace('"', "")
    name = name[: min(len(name), 8)]
    artist_name = artists_api.get_artist(name)
    d["artist_name"] = artist_name.replace("'", "").replace('"', "")
    d["image_url"] = get_thumbnail(venue_name).replace("'", "\\'").replace('"', '\\"')
    return d


def test_wiki():
    subject = "Drake"
    url = "https://en.wikipedia.org/w/api.php"
    params = {
        "action": "query",
        "format": "json",
        "titles": subject,
        "prop": "images",
    }

    response = requests.get(url, params=params)
    data = response.json()


def test_musicbrainz():
    q = "drake"
    url = "https://musicbrainz.org/ws/2/artist"
    params = {"query": q, "limit": 1, "fmt": "json"}

    url = "https://musicbrainz.org/ws/2/artist/9fff2f8a-21e6-47de-a2b8-7f449929d43f"
    params = {"fmt": "json"}
    response = requests.get(url, params=params)

    data = response.json()


def test_bing():
    url = "https://api.bing.microsoft.com/v7.0/images/search"

    q = "madison square garden"
    q = "63 Meadow Street\nBrooklyn\nUnited States of America"
    # q = q[:min(len(q), 15)]
    headers = {"Ocp-Apim-Subscription-Key": bing_token}

    params = {"q": q, "count": 1, "mkt": "en-US"}
    response = requests.get(url, params=params, headers=headers)
    data = response.json()

    thumbnail = data["value"][0]["thumbnailUrl"].strip()
    # print(thumbnail)


def get_concerts(n=10, offset=0):
    raw_concerts = __get_raw_concerts(n, offset)
    concerts = []
    for rc in raw_concerts:
        try:
            concerts.append(__extract_fields(rc))
            time.sleep(0.011)
        except:
            continue

    result = {"concerts": concerts}
    return result


def __get_concert_by_name(name):
    response = requests.get(
        url="https://api.predicthq.com/v1/events/",
        headers={"Authorization": f"Bearer {pmq_tkn}", "Accept": "application/json"},
        params={
            "sort": "rank",
            "limit": 1,
            "category": "concerts,festivals,sports,conferences,expos,performing-arts,community",
            "q": name,
        },
    )
    top_result = response.json()["results"][0]
    concert_dict = __extract_fields(top_result)
    return concert_dict


# THIS IS THE PUBLIC FUNCTION
def get_concerts_by_name(names):
    concerts = []
    for name in names:
        concerts.append(__get_concert_by_name(name))
    result = {"concerts": concerts}
    return json.dumps(result, indent=2)


def test():
    print(get_concerts(4))
    # print(__get_concert_by_name('harry styles'))
    # res = get_concerts_by_name(['harry styles', 'MarShMEllo', 'Post malone'])
    # print(res)
    # get_concerts(10)


if __name__ == "__main__":
    test()
    # test_wiki()
    # test_musicbrainz()
    # test_bing()
