import unittest
import requests


class TestBackend(unittest.TestCase):
    # Artist Tests
    def test_all_artists(self):
        response = requests.get(url="http://api.concertfor.me/api/artists").json()
        self.assertTrue(response["artists"])

    def test_artist_by_id(self):
        response = requests.get(url="http://api.concertfor.me/api/artists/1").json()
        self.assertTrue(response["artists"])

    def test_filter_artists(self):
        response = requests.get(
            url="http://api.concertfor.me/api/artists?n=10&offset=0&filterby=home_country+followers&fquery=US+>10000000"
        ).json()
        self.assertTrue(response["artists"])

    def test_sort_artists(self):
        response = requests.get(
            url="http://api.concertfor.me/api/artists?n=10&offset=0&sortby=-followers"
        ).json()
        self.assertTrue(response["artists"])

    def test_search_artists(self):
        response = requests.get(
            url="http://api.concertfor.me/api/artists?n=10&offset=0&search=Red+Hot"
        ).json()
        self.assertTrue(response["artists"])

    def test_compound_query_artists(self):
        response = requests.get(
            url="http://api.concertfor.me/api/artists?n=10&offset=0&search=Red+Hot&filterby=home_country+followers&fquery=US+>10000000&sortby=-followers"
        ).json()
        self.assertTrue(response["artists"])

    # Concert tests
    def test_all_concerts(self):
        response = requests.get(url="http://api.concertfor.me/api/concerts").json()
        self.assertTrue(response["concerts"])

    def test_concert_by_id(self):
        response = requests.get(url="http://api.concertfor.me/api/concerts/1").json()
        self.assertTrue(response["concerts"])

    def test_filter_concerts(self):
        response = requests.get(
            url="http://api.concertfor.me/api/concerts?n=10&offset=0&filterby=time_zone+predicted_attendance+predicted_attendance&fquery=New_York+>33000+<34700"
        ).json()
        self.assertTrue(response["concerts"])

    def test_sort_concerts(self):
        response = requests.get(
            url="http://api.concertfor.me/api/concerts?n=10&offset=0&sortby=+predicted_attendance"
        ).json()
        self.assertTrue(response["concerts"])

    def test_search_concerts(self):
        response = requests.get(
            url="http://api.concertfor.me/api/concerts?n=10&offset=0&search=The+Meadows"
        ).json()
        self.assertTrue(response["concerts"])

    def test_compound_query_concerts(self):
        response = requests.get(
            url="http://api.concertfor.me/api/concerts?n=10&offset=0&search=The+Meadows&filterby=time_zone+predicted_attendance+predicted_attendance&fquery=New_York+>33000+<34700&sortby=+predicted_attendance"
        ).json()
        self.assertTrue(response["concerts"])

    # Songs
    def test_all_songs(self):
        response = requests.get(url="http://api.concertfor.me/api/songs").json()
        self.assertTrue(response["songs"])

    def test_song_by_id(self):
        response = requests.get(url="http://api.concertfor.me/api/songs/1").json()
        self.assertTrue(response["songs"])

    def test_filter_songs(self):
        response = requests.get(
            url="http://api.concertfor.me/api/songs?n=10&offset=0&filterby=explicit+track_length&fquery=notExplicit+<150000"
        ).json()
        self.assertTrue(response["songs"])

    def test_sort_songs(self):
        response = requests.get(
            url="http://api.concertfor.me/api/songs?n=10&offset=0&sortby=+track_length"
        ).json()
        self.assertTrue(response["songs"])

    def test_search_songs(self):
        response = requests.get(
            url="http://api.concertfor.me/api/songs?n=10&offset=0&search=you"
        ).json()
        self.assertTrue(response["songs"])

    def test_compound_query_songs(self):
        response = requests.get(
            url="http://api.concertfor.me/api/songs?n=10&offset=0&search=you&filterby=explicit+track_length&fquery=notExplicit+<150000&sortby=+track_length"
        ).json()
        self.assertTrue(response["songs"])


if __name__ == "__main__":
    unittest.main()
